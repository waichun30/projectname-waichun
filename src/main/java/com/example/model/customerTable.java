/*

This class create database with the following structure

id Long Unique PK auto_increament +1
name String
age int

*/
package com.example.model;

import javax.persistence.*;
import java.io.Serializable;


//modified at 10:48am
@Entity
@Table(name = "customerTable")
public class customerTable implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "age")
    private int age;

    // Constructor
    public customerTable(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public customerTable() {
    }


    // Getter and Setter
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}