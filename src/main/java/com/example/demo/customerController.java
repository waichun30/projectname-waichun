/*
* Controller file
* Manage method
*
* */


package com.example.demo;

import com.example.repo.customerRep;
import com.example.model.customerTable;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping(value = "/customer")
public class customerController {



    @Resource
    @Qualifier("contactService")
    private customerRep customerRep;

    public customerController() {
    }



    /*@GetMapping("/{id}")
    public customer getById(@PathVariable String id){

        boolean exists=false;
        customer a =new customer();
        for(int i=0;i<customerList.size();i++){
            a=customerList.get(i);
            if(a.getId()==Integer.parseInt(id)) {
                exists = true;
                break;
            }
        }

        if(exists)
            return a;
        return a;
    }*/

    // GET method to display
    @RequestMapping(value="/all",method= RequestMethod.GET)
    public List<customerTable> getAll(){
        return customerRep.findAll();
    }

    // Get Method, retrieve the saved data
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public customerTable getUser(@PathVariable("id") Long id) {
        return customerRep.findById(id).orElseThrow(() -> new Exception("Customer", "id", id));
    }



    // POST method, save name and age to database,
    // How to insert with GUI?
    @RequestMapping(value = "/{name},{age}", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<String> addCustomer(@PathVariable("name") String name,@PathVariable("age") int age) {
            customerTable customerTable=new customerTable(name,age);
            customerRep.save(customerTable);
            return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<String> addCustomer2(@RequestBody customerTable customerTable) {
        //customerTable customerTable=new customerTable();
        customerRep.save(customerTable);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    // PUT method, alter customer age +1
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    public customerTable alterAge(@PathVariable("id") Long id) {
        customerTable c = customerRep.findById(id)
                .orElseThrow(() -> new Exception("Customer", "id", id));
        c.setAge(c.getAge()+1);
        customerTable updatedNote = customerRep.save(c);
        return updatedNote;
    }

    // Delete method, delete the saved method based on customer id
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    public List<customerTable> deleteCustomer(@PathVariable("id") Long id) {
        customerTable c = customerRep.findById(id)
                .orElseThrow(() -> new Exception("Customer", "id", id));
        customerRep.delete(c);
        return customerRep.findAll();
    }

}
