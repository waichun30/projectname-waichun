package com.example.repo;

import com.example.model.customerTable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface customerRep extends JpaRepository<customerTable, Long> {
}